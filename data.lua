
local boiler = table.deepcopy(data.raw.boiler.boiler)
boiler.name = 'pressurized-boilers-steel-boiler'
boiler.max_health = boiler.max_health * 1.25

boiler.energy_source.emissions_per_minute['pollution'] = boiler.energy_source.emissions_per_minute['pollution'] * 1.75

for i, data in ipairs(boiler.resistances) do
	if data.type == 'impact' or data.type == 'explosion' then
		data.percent = data.percent * 1.2
	end

	if data.type == 'fire' then
		data.percent = 95
	end
end

do
	local subNum = string.match(boiler.energy_consumption, '^[0-9%.]+')
	local success = false

	if subNum then
		local subNumP = tonumber(subNum)

		if subNumP then
			local subType = string.sub(boiler.energy_consumption, #subNum + 1)

			boiler.energy_consumption = (subNumP * 2) .. subType
			success = true
		end
	end

	if not success then
		boiler.energy_consumption = '3.6MW'
	end
end

boiler.target_temperature = (boiler.target_temperature - 15) * 2 + 15

boiler.minable = {mining_time = 0.2, result = 'pressurized-boilers-steel-boiler'}

if data.raw['fuel-category']['processed-chemical'] then
	boiler.energy_source.fuel_categories = boiler.energy_source.fuel_categories or {boiler.energy_source.fuel_category or 'chemical'}
	boiler.energy_source.fuel_category = nil

	local hit = false

	for _, name in ipairs(boiler.energy_source.fuel_categories) do
		if name == 'processed-chemical' then
			hit = true
			break
		end
	end

	if not hit then
		table.insert(boiler.energy_source.fuel_categories, 'processed-chemical')
	end
end

if data.raw['fuel-category']['vehicle-fuel'] then
	boiler.energy_source.fuel_categories = boiler.energy_source.fuel_categories or {boiler.energy_source.fuel_category or 'chemical'}
	boiler.energy_source.fuel_category = nil

	local hit = false

	for _, name in ipairs(boiler.energy_source.fuel_categories) do
		if name == 'vehicle-fuel' then
			hit = true
			break
		end
	end

	if not hit then
		table.insert(boiler.energy_source.fuel_categories, 'vehicle-fuel')
	end
end

local steam_engine = table.deepcopy(data.raw.generator['steam-engine'])
steam_engine.name = 'pressurized-boilers-steel-engine'
steam_engine.max_health = steam_engine.max_health * 1.25
steam_engine.maximum_temperature = boiler.target_temperature

steam_engine.minable = {mining_time = 0.3, result = 'pressurized-boilers-steel-engine'}

if mods['Krastorio2'] then
	steam_engine.fluid_usage_per_tick = 0.5 / 3
	boiler.energy_consumption = '3MW'
	boiler.energy_source.emissions_per_minute = 30
end

local boiler_recipe = {
	type = 'recipe',
	name = 'pressurized-boilers-steel-boiler',
	results = {{type = 'item', name = 'pressurized-boilers-steel-boiler', amount = 1}},

	ingredients = {
		{type = 'item', name = 'pipe', amount = 4},
		{type = 'item', name = 'steel-furnace', amount = 1},
	},

	enabled = false,
}

local steam_engine_recipe = {
	type = 'recipe',
	name = 'pressurized-boilers-steel-engine',
	results = {{type = 'item', name = 'pressurized-boilers-steel-engine', amount = 1}},

	ingredients = {
		{type = 'item', name = mods['Krastorio2'] and 'steel-gear-wheel' or 'iron-gear-wheel', amount = mods['Krastorio2'] and 8 or 12},
        {type = 'item', name = mods['aai-industry'] and 'electric-motor' or 'pipe', amount = mods['aai-industry'] and 4 or 5},
        {type = 'item', name = 'steel-plate', amount = 10}
	},

	enabled = false,
}

local effects = data.raw.technology['advanced-material-processing'].effects

table.insert(effects, {
	type = 'unlock-recipe',
	recipe = 'pressurized-boilers-steel-boiler'
})

table.insert(effects, {
	type = 'unlock-recipe',
	recipe = 'pressurized-boilers-steel-engine'
})

local boiler_item = table.deepcopy(data.raw.item['boiler'])
local engine_item = table.deepcopy(data.raw.item['steam-engine'])

boiler_item.name = 'pressurized-boilers-steel-boiler'
boiler_item.place_result = 'pressurized-boilers-steel-boiler'
boiler_item.order = 'b[steam-power]-b[steam-engine]-1'

engine_item.name = 'pressurized-boilers-steel-engine'
engine_item.place_result = 'pressurized-boilers-steel-engine'
engine_item.order = 'b[steam-power]-b[steam-engine]-2'

data:extend({boiler, steam_engine, boiler_item, engine_item, boiler_recipe, steam_engine_recipe})
